<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PostCreateRequest;
use App\Models\Post;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostsController extends Controller
{
    public function index()
    {
        $posts = Post::get();//Post::where('wallet','>',100000)->get();

        return view('admin.posts.list', compact('posts'));
    }

    public function create()
    {
        $postStatuses = Post::postStatuses();

        return view('admin.posts.create', compact('postStatuses'));
    }

    public function store(PostCreateRequest $request)
    {

        $post = Post::create([
            'post_title'   => $request->input('postTitle'),
            'post_slug'    => $request->input('postTitle'),
            'post_content' => $request->input('postContent'),
            'post_author'  => User::first()->id, //Auth::id(),
            'post_status'  => $request->input('postStatus')
        ]);
        if ($post && $post instanceof Post) {
            return back()->with('status', 'مطلب جدید با موفقیت ایجاد گردید!');
        }
    }

    public function delete(Request $request, $post_id)
    {

        $post         = Post::find($post_id);
        $deleteResult = $post->delete();
        if ($deleteResult) {
            return back()->with('status', 'کاربر با موفقیت حذف گردید');
        }
//        Post::destroy($post_id);
    }

    public function edit(Request $request, $post_id)
    {

        $post      = Post::find($post_id);
        $postRoles = Post::getPostRoles();

        return view('admin.posts.edit', compact('post', 'postRoles'));

    }

    public function update(Request $request, $post_id)
    {
        $post = Post::find($post_id);
        if ($post && $post instanceof Post) {
            $postData = [
                'name'  => $request->input('postFullName'),
                'email' => $request->input('postEmail'),
            ];
            if ($request->filled('postPassword')) {
                $postData['password'] = $request->input('postPassword');
            }
            $updateResult = $post->update($postData);
            if ($updateResult) {
                return redirect()->route('admin.posts')->with('status', 'کاربر با موفقیت به روز رسانی گردید!');
            }
        }
    }
}
